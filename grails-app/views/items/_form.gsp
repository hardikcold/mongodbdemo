<%@ page import="com.mangodemo.domains.Items" %>



<div class="fieldcontain ${hasErrors(bean: itemsInstance, field: 'itemImg', 'error')} ">
	<label for="itemImg">
		<g:message code="items.itemImg.label" default="Item Img" />
		
	</label>
	<input type="file" id="itemImg" name="itemImg" />

</div>

<div class="fieldcontain ${hasErrors(bean: itemsInstance, field: 'itemName', 'error')} ">
	<label for="itemName">
		<g:message code="items.itemName.label" default="Item Name" />
		
	</label>
	<g:textField name="itemName" value="${itemsInstance?.itemName}" />

</div>

<div class="fieldcontain ${hasErrors(bean: itemsInstance, field: 'person', 'error')} ">
	<label for="person">
		<g:message code="items.person.label" default="Person" />
		
	</label>
	<g:select id="person" name="person.id" from="${com.mangodemo.domains.Person.list()}" optionKey="id" required="" value="${itemsInstance?.person?.id}" class="many-to-one"/>

</div>

