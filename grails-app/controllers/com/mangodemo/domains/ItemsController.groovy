package com.mangodemo.domains



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ItemsController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Items.list(params), model:[itemsInstanceCount: Items.count()]
    }

    def show(Items itemsInstance) {
        respond itemsInstance
    }

    def create() {
        respond new Items(params)
    }
	
	def showItemImage() {
		def fileInstance = Items.get(params.id)
		response.outputStream << fileInstance.itemImg // write the image to the outputstream
		response.outputStream.flush()
	}

    @Transactional
    def save(Items itemsInstance) {
        if (itemsInstance == null) {
            notFound()
            return
        }

        if (itemsInstance.hasErrors()) {
            respond itemsInstance.errors, view:'create'
            return
        }

        itemsInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'itemsInstance.label', default: 'Items'), itemsInstance.id])
                redirect itemsInstance
            }
            '*' { respond itemsInstance, [status: CREATED] }
        }
    }

    def edit(Items itemsInstance) {
        respond itemsInstance
    }

    @Transactional
    def update(Items itemsInstance) {
        if (itemsInstance == null) {
            notFound()
            return
        }

        if (itemsInstance.hasErrors()) {
            respond itemsInstance.errors, view:'edit'
            return
        }

        itemsInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Items.label', default: 'Items'), itemsInstance.id])
                redirect itemsInstance
            }
            '*'{ respond itemsInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Items itemsInstance) {

        if (itemsInstance == null) {
            notFound()
            return
        }

        itemsInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Items.label', default: 'Items'), itemsInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'itemsInstance.label', default: 'Items'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
