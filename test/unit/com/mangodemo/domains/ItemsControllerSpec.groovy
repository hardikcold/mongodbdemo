package com.mangodemo.domains



import grails.test.mixin.*
import spock.lang.*

@TestFor(ItemsController)
@Mock(Items)
class ItemsControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.itemsInstanceList
            model.itemsInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.itemsInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            def items = new Items()
            items.validate()
            controller.save(items)

        then:"The create view is rendered again with the correct model"
            model.itemsInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            items = new Items(params)

            controller.save(items)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/items/show/1'
            controller.flash.message != null
            Items.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def items = new Items(params)
            controller.show(items)

        then:"A model is populated containing the domain instance"
            model.itemsInstance == items
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def items = new Items(params)
            controller.edit(items)

        then:"A model is populated containing the domain instance"
            model.itemsInstance == items
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/items/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def items = new Items()
            items.validate()
            controller.update(items)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.itemsInstance == items

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            items = new Items(params).save(flush: true)
            controller.update(items)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/items/show/$items.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/items/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def items = new Items(params).save(flush: true)

        then:"It exists"
            Items.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(items)

        then:"The instance is deleted"
            Items.count() == 0
            response.redirectedUrl == '/items/index'
            flash.message != null
    }
}
